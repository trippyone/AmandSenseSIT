﻿using System;
using System.Reflection;
using Aki.Reflection.Patching;
using UnityEngine;

namespace AmandSenseSIT.Patches
{
    public class AmandsSensePrismEffectsPatch : ModulePatch
    {
        protected override MethodBase GetTargetMethod()
        {
            MethodBase mBase = null;
            try
            {
                mBase = typeof(PrismEffects).GetMethod("OnEnable", BindingFlags.Instance | BindingFlags.NonPublic);
                Debug.LogError(mBase);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }

            return mBase;
        }

        [PatchPostfix]
        private static void PatchPostFix(ref PrismEffects __instance)
        {
            try
            {
                if (__instance.gameObject.name == "FPS Camera") AmandsSenseClass.prismEffects = __instance;
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
            
        }
    }
}