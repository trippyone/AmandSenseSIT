﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Aki.Reflection.Patching;
using EFT;
using UnityEngine;
using SIT.Core.Coop;

namespace AmandSenseSIT.Patches
{
    public class AmandsLocalPlayerPatch : ModulePatch
    {
        protected override MethodBase GetTargetMethod()
        {
            MethodBase mBase = null;
            try
            {
                mBase = typeof(CoopGame).GetMethod("vmethod_2", BindingFlags.Instance | BindingFlags.Public);
                Debug.LogError(mBase);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }

            return mBase;
        }

        private static async Task WaitForCoopGame(Task<LocalPlayer> task)
        {
            task.Wait();
            LocalPlayer localPlayer = task.Result;
            if (localPlayer != null && localPlayer.IsYourPlayer)
            {
                Logger.LogDebug(localPlayer.name);
                Logger.LogDebug(localPlayer.Position.x);
                AmandsSenseClass.localPlayer = localPlayer;
                AmandsSenseClass.ItemsSenses.Clear();
                AmandsSenseClass.ItemsAlwaysOn.Clear();
                AmandsSenseClass.ContainersAlwaysOn.Clear();
                AmandsSenseClass.DeadbodyAlwaysOn.Clear();
                AmandsSensePlugin.AmandsSenseClassComponent.DynamicAlwaysOnSense();
            }
        }

        [PatchPostfix]
        private static void PatchPostFix(Task<LocalPlayer> __result)
        {
            try
            {
                Task.Run(() => WaitForCoopGame(__result));
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }
    }
}