﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using AmandSenseSIT.Types;
using EFT;
using EFT.Interactive;
using EFT.InventoryLogic;
using HarmonyLib;
using UnityEngine;

namespace AmandSenseSIT.Sense.DeadBody
{
    public class AmandsSenseDeadbody : MonoBehaviour
    {
        public Corpse corpse;
        public BodyPartCollider bodyPartCollider;
        public bool emptyCorpse = true;
        public string Id;
        public SpriteRenderer spriteRenderer;
        public Sprite sprite;
        public Light light;
        public Color color = AmandsSensePlugin.LootableContainerColor.Value;
        public bool useNewSize = false;

        public float Delay = 0f;
        private bool UpdateOpacity = false;
        private bool StartOpacity = true;
        private float Opacity = 0f;

        public void Start()
        {
            try
            {
                if (AmandsSenseClass.ItemsSenses.Contains(Id))
                {
                    Destroy(gameObject);
                }
                else
                {
                    AmandsSenseClass.ItemsSenses.Add(Id);
                    WaitAndStart();
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }

        private async void WaitAndStart()
        {
            try
            {
                await Task.Delay((int)(Delay * 1000));
                if (corpse != null && corpse.gameObject.activeSelf && bodyPartCollider != null &&
                    bodyPartCollider.gameObject.activeSelf && bodyPartCollider.Collider != null &&
                    AmandsSenseClass.localPlayer != null &&
                    bodyPartCollider.Collider.transform.position.y >
                    AmandsSenseClass.localPlayer.Position.y + AmandsSensePlugin.MinHeight.Value &&
                    bodyPartCollider.Collider.transform.position.y <
                    AmandsSenseClass.localPlayer.Position.y + AmandsSensePlugin.MaxHeight.Value)
                {
                    gameObject.transform.position = bodyPartCollider.Collider.transform.position +
                                                    Vector3.up * AmandsSensePlugin.NormalSize.Value + Vector3.up * 0.5f;
                    var eSenseItemColor = ESenseItemColor.Default;
                    if (AmandsSenseClass.itemsJsonClass != null && AmandsSenseClass.itemsJsonClass.RareItems != null &&
                        AmandsSenseClass.itemsJsonClass.KappaItems != null &&
                        AmandsSenseClass.itemsJsonClass.NonFleaExclude != null &&
                        AmandsSenseClass.localPlayer != null &&
                        AmandsSenseClass.localPlayer.Profile != null &&
                        AmandsSenseClass.localPlayer.Profile.WishList != null)
                    {
                        var localPlayer = corpse.gameObject.GetComponent<LocalPlayer>();
                        if (localPlayer != null && localPlayer.Profile != null)
                        {
                            var Inventory = Traverse.Create(localPlayer.Profile).Field("Inventory").GetValue();
                            if (Inventory != null)
                            {
                                var AllRealPlayerItems = Traverse.Create(Inventory).Property("AllRealPlayerItems")
                                    .GetValue<IEnumerable<EFT.InventoryLogic.Item>>();
                                if (AllRealPlayerItems != null)
                                    foreach (var item in AllRealPlayerItems)
                                    {
                                        if (item.Parent != null)
                                        {
                                            if (item.Parent.Container != null &&
                                                item.Parent.Container.ParentItem != null &&
                                                GClass2552.TypeTable["5448bf274bdc2dfc2f8b456a"]
                                                    .IsAssignableFrom(item.Parent.Container.ParentItem.GetType()))
                                                continue;
                                            var slot = item.Parent.Container as Slot;
                                            if (slot != null)
                                            {
                                                if (slot.Name == "Dogtag") continue;
                                                if (slot.Name == "SecuredContainer") continue;
                                                if (slot.Name == "Scabbard") continue;
                                                if (slot.Name == "ArmBand") continue;
                                            }
                                        }

                                        if (emptyCorpse) emptyCorpse = false;
                                        if (AmandsSenseClass.itemsJsonClass.RareItems.Contains(item.TemplateId))
                                        {
                                            eSenseItemColor = ESenseItemColor.Rare;
                                        }
                                        else if (AmandsSenseClass.localPlayer.Profile.WishList
                                                     .Contains(item.TemplateId) &&
                                                 eSenseItemColor != ESenseItemColor.Rare)
                                        {
                                            eSenseItemColor = ESenseItemColor.WishList;
                                        }
                                        else if (item.Template != null && !item.Template.CanSellOnRagfair &&
                                                 !AmandsSenseClass.itemsJsonClass.NonFleaExclude
                                                     .Contains(item.TemplateId) &&
                                                 eSenseItemColor != ESenseItemColor.Rare &&
                                                 eSenseItemColor != ESenseItemColor.WishList)
                                        {
                                            if (!AmandsSensePlugin.NonFleaAmmo.Value && GClass2552
                                                    .TypeTable["5485a8684bdc2da71d8b4567"]
                                                    .IsAssignableFrom(item.GetType()))
                                                continue;
                                            else
                                                eSenseItemColor = ESenseItemColor.NonFlea;
                                        }
                                        else if (AmandsSenseClass.itemsJsonClass.KappaItems.Contains(item.TemplateId) &&
                                                 eSenseItemColor == ESenseItemColor.Default)
                                        {
                                            eSenseItemColor = ESenseItemColor.Kappa;
                                        }
                                    }
                            }
                        }
                    }

                    if (!emptyCorpse)
                    {
                        switch (corpse.Side)
                        {
                            case EPlayerSide.Usec:
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("Usec.png"))
                                    sprite = AmandsSenseClass.LoadedSprites["Usec.png"];
                                break;
                            case EPlayerSide.Bear:
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("Bear.png"))
                                    sprite = AmandsSenseClass.LoadedSprites["Bear.png"];
                                break;
                            case EPlayerSide.Savage:
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_kills_big.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_kills_big.png"];
                                    useNewSize = true;
                                }

                                break;
                        }

                        AmandsSenseClass.onContainerSensesAdded(Id, gameObject.transform.position);
                        switch (eSenseItemColor)
                        {
                            case ESenseItemColor.Kappa:
                                color = AmandsSensePlugin.KappaItemsColor.Value;
                                break;
                            case ESenseItemColor.NonFlea:
                                color = AmandsSensePlugin.NonFleaItemsColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_barter.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_barter.png"];
                                    useNewSize = true;
                                }

                                break;
                            case ESenseItemColor.WishList:
                                color = AmandsSensePlugin.WishListItemsColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_fav_checked.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_fav_checked.png"];
                                    useNewSize = true;
                                }

                                break;
                            case ESenseItemColor.Rare:
                                color = AmandsSensePlugin.RareItemsColor.Value;
                                break;
                        }

                        spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                        if (spriteRenderer != null)
                        {
                            light = gameObject.AddComponent<Light>();
                            if (light != null)
                            {
                                light.color = new Color(color.r, color.g, color.b, 1f);
                                light.shadows = LightShadows.None;
                                light.intensity = 0f;
                                light.range = AmandsSensePlugin.LightRange.Value;
                            }

                            spriteRenderer.sprite = sprite;
                            spriteRenderer.color = new Color(color.r, color.g, color.b, 0f);
                            transform.LookAt(Camera.main.transform.position, Vector3.up);
                            transform.localScale =
                                (useNewSize ? AmandsSensePlugin.NewSize.Value : AmandsSensePlugin.Size.Value) *
                                Mathf.Min(AmandsSensePlugin.SizeClamp.Value,
                                    Vector3.Distance(Camera.main.transform.position, transform.position));
                            UpdateOpacity = true;
                            await Task.Delay((int)(AmandsSensePlugin.Duration.Value * 1000));
                            UpdateOpacity = true;
                            StartOpacity = false;
                        }
                        else
                        {
                            AmandsSenseClass.ItemsSenses.Remove(Id);
                            Destroy(gameObject);
                        }
                    }
                    else
                    {
                        AmandsSenseClass.ItemsSenses.Remove(Id);
                        Destroy(gameObject);
                    }
                }
                else
                {
                    AmandsSenseClass.ItemsSenses.Remove(Id);
                    Destroy(gameObject);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }

        public void Update()
        {
            try
            {
                if (Camera.main != null)
                {
                    transform.LookAt(Camera.main.transform.position, Vector3.up);
                    transform.localScale = (useNewSize ? AmandsSensePlugin.NewSize.Value : AmandsSensePlugin.Size.Value) *
                                           Mathf.Min(AmandsSensePlugin.SizeClamp.Value,
                                               Vector3.Distance(Camera.main.transform.position, transform.position));
                }

                if (UpdateOpacity)
                {
                    if (StartOpacity)
                    {
                        Opacity += AmandsSensePlugin.OpacitySpeed.Value * Time.deltaTime;
                        if (Opacity >= 1f)
                        {
                            UpdateOpacity = false;
                            StartOpacity = false;
                        }
                    }
                    else
                    {
                        Opacity -= AmandsSensePlugin.OpacitySpeed.Value * Time.deltaTime;
                        if (Opacity <= 0f)
                        {
                            UpdateOpacity = false;
                            AmandsSenseClass.ItemsSenses.Remove(Id);
                            Destroy(gameObject);
                        }
                    }

                    if (spriteRenderer != null && light != null)
                    {
                        spriteRenderer.color = new Color(color.r, color.g, color.b, color.a * Opacity);
                        light.intensity = AmandsSensePlugin.LightIntensity.Value * Opacity;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }
    }
}