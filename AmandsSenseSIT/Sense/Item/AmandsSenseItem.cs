﻿using System.Linq;
using System.Threading.Tasks;
using AmandSenseSIT.Types;
using Comfort.Common;
using EFT.Interactive;
using EFT.InventoryLogic;
using EFT.UI;
using UnityEngine;

namespace AmandSenseSIT.Sense.Item
{
    public class AmandsSenseItem : MonoBehaviour
    {
        public ObservedLootItem observedLootItem;
        public string Id;
        public SpriteRenderer spriteRenderer;
        public Sprite sprite;
        public string ItemSound;
        public Light light;
        public Color color = AmandsSensePlugin.ObservedLootItemColor.Value;
        public bool useNewSize = false;

        public float Delay = 0f;
        private bool UpdateOpacity = false;
        private bool StartOpacity = true;
        private float Opacity = 0f;

        public void Start()
        {
            if (AmandsSenseClass.ItemsSenses.Contains(Id))
            {
                Destroy(gameObject);
            }
            else
            {
                AmandsSenseClass.ItemsSenses.Add(Id);
                WaitAndStart();
            }
        }

        private async void WaitAndStart()
        {
            await Task.Delay((int)(Delay * 1000));
            if (observedLootItem != null && observedLootItem.gameObject.activeSelf && observedLootItem.Item != null &&
                AmandsSenseClass.localPlayer != null &&
                observedLootItem.transform.position.y >
                AmandsSenseClass.localPlayer.Position.y + AmandsSensePlugin.MinHeight.Value &&
                observedLootItem.transform.position.y <
                AmandsSenseClass.localPlayer.Position.y + AmandsSensePlugin.MaxHeight.Value)
            {
                var boxCollider = observedLootItem.gameObject.GetComponent<BoxCollider>();
                if (boxCollider != null)
                {
                    var position = boxCollider.transform.TransformPoint(boxCollider.center);
                    gameObject.transform.position = new Vector3(position.x,
                        boxCollider.ClosestPoint(position + Vector3.up * 100f).y + AmandsSensePlugin.NormalSize.Value,
                        position.z);
                }
                else
                {
                    gameObject.transform.position = observedLootItem.transform.position +
                                                    Vector3.up * AmandsSensePlugin.NormalSize.Value;
                }

                var eSenseItemType = ESenseItemType.ObservedLootItem;
                eSenseItemType = AmandsSenseClass.SenseItemType(observedLootItem.Item.GetType());
                if (typeof(Weapon).IsAssignableFrom(observedLootItem.Item.GetType()))
                {
                    var weapon = observedLootItem.Item as Weapon;
                    if (weapon != null)
                        switch (weapon.WeapClass)
                        {
                            case "assaultCarbine":
                                eSenseItemType = ESenseItemType.AssaultCarbines;
                                color = AmandsSensePlugin.AssaultCarbinesColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_weapons_carbines.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_weapons_carbines.png"];
                                    useNewSize = true;
                                }

                                break;
                            case "assaultRifle":
                                eSenseItemType = ESenseItemType.AssaultRifles;
                                color = AmandsSensePlugin.AssaultRiflesColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_weapons_assaultrifles.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_weapons_assaultrifles.png"];
                                    useNewSize = true;
                                }

                                break;
                            case "sniperRifle":
                                eSenseItemType = ESenseItemType.BoltActionRifles;
                                color = AmandsSensePlugin.BoltActionRiflesColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_weapons_botaction.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_weapons_botaction.png"];
                                    useNewSize = true;
                                }

                                break;
                            case "grenadeLauncher":
                                eSenseItemType = ESenseItemType.GrenadeLaunchers;
                                color = AmandsSensePlugin.GrenadeLaunchersColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_weapons_gl.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_weapons_gl.png"];
                                    useNewSize = true;
                                }

                                break;
                            case "machinegun":
                                eSenseItemType = ESenseItemType.MachineGuns;
                                color = AmandsSensePlugin.MachineGunsColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_weapons_mg.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_weapons_mg.png"];
                                    useNewSize = true;
                                }

                                break;
                            case "marksmanRifle":
                                eSenseItemType = ESenseItemType.MarksmanRifles;
                                color = AmandsSensePlugin.MarksmanRiflesColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_weapons_dmr.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_weapons_dmr.png"];
                                    useNewSize = true;
                                }

                                break;
                            case "pistol":
                                eSenseItemType = ESenseItemType.Pistols;
                                color = AmandsSensePlugin.PistolsColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_weapons_pistols.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_weapons_pistols.png"];
                                    useNewSize = true;
                                }

                                break;
                            case "smg":
                                eSenseItemType = ESenseItemType.SMGs;
                                color = AmandsSensePlugin.SMGsColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_weapons_smg.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_weapons_smg.png"];
                                    useNewSize = true;
                                }

                                break;
                            case "shotgun":
                                eSenseItemType = ESenseItemType.Shotguns;
                                color = AmandsSensePlugin.ShotgunsColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_weapons_shotguns.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_weapons_shotguns.png"];
                                    useNewSize = true;
                                }

                                break;
                            case "specialWeapon":
                                eSenseItemType = ESenseItemType.SpecialWeapons;
                                color = AmandsSensePlugin.SpecialWeaponsColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_weapons_special.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_weapons_special.png"];
                                    useNewSize = true;
                                }

                                break;
                        }
                }

                if (eSenseItemType == ESenseItemType.All) eSenseItemType = ESenseItemType.ObservedLootItem;
                AmandsSenseClass.onItemsSensesAdded(Id, observedLootItem.Item.TemplateId,
                    observedLootItem.Item.CanSellOnRagfair, gameObject.transform.position, eSenseItemType);
                switch (eSenseItemType)
                {
                    case ESenseItemType.ObservedLootItem:
                        color = AmandsSensePlugin.ObservedLootItemColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("ObservedLootItem.png"))
                            sprite = AmandsSenseClass.LoadedSprites["ObservedLootItem.png"];
                        break;
                    case ESenseItemType.Others:
                        color = AmandsSensePlugin.OthersColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_barter_others.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_barter_others.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.BuildingMaterials:
                        color = AmandsSensePlugin.BuildingMaterialsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_barter_building.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_barter_building.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Electronics:
                        color = AmandsSensePlugin.ElectronicsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_barter_electronics.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_barter_electronics.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.EnergyElements:
                        color = AmandsSensePlugin.EnergyElementsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_barter_energy.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_barter_energy.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.FlammableMaterials:
                        color = AmandsSensePlugin.FlammableMaterialsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_barter_flammable.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_barter_flammable.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.HouseholdMaterials:
                        color = AmandsSensePlugin.HouseholdMaterialsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_barter_household.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_barter_household.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.MedicalSupplies:
                        color = AmandsSensePlugin.MedicalSuppliesColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_barter_medical.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_barter_medical.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Tools:
                        color = AmandsSensePlugin.ToolsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_barter_tools.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_barter_tools.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Valuables:
                        color = AmandsSensePlugin.ValuablesColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_barter_valuables.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_barter_valuables.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Backpacks:
                        color = AmandsSensePlugin.BackpacksColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_gear_backpacks.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_gear_backpacks.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.BodyArmor:
                        color = AmandsSensePlugin.BodyArmorColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_gear_armor.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_gear_armor.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Eyewear:
                        color = AmandsSensePlugin.EyewearColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_gear_visors.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_gear_visors.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Facecovers:
                        color = AmandsSensePlugin.FacecoversColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_gear_facecovers.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_gear_facecovers.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.GearComponents:
                        color = AmandsSensePlugin.GearComponentsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_gear_components.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_gear_components.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Headgear:
                        color = AmandsSensePlugin.HeadgearColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_gear_headwear.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_gear_headwear.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Headsets:
                        color = AmandsSensePlugin.HeadsetsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_gear_headsets.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_gear_headsets.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.SecureContainers:
                        color = AmandsSensePlugin.SecureContainersColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_gear_secured.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_gear_secured.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.StorageContainers:
                        color = AmandsSensePlugin.StorageContainersColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_gear_cases.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_gear_cases.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.TacticalRigs:
                        color = AmandsSensePlugin.TacticalRigsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_gear_rigs.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_gear_rigs.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.FunctionalMods:
                        color = AmandsSensePlugin.FunctionalModsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_mods_functional.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_mods_functional.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.GearMods:
                        color = AmandsSensePlugin.GearModsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_mods_gear.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_mods_gear.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.VitalParts:
                        color = AmandsSensePlugin.VitalPartsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_mods_vital.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_mods_vital.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.MeleeWeapons:
                        color = AmandsSensePlugin.MeleeWeaponsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_weapons_melee.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_weapons_melee.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Throwables:
                        color = AmandsSensePlugin.ThrowablesColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_weapons_throw.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_weapons_throw.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.AmmoPacks:
                        color = AmandsSensePlugin.AmmoPacksColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_ammo_boxes.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_ammo_boxes.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Rounds:
                        color = AmandsSensePlugin.RoundsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_ammo_rounds.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_ammo_rounds.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Drinks:
                        color = AmandsSensePlugin.DrinksColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_provisions_drinks.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_provisions_drinks.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Food:
                        color = AmandsSensePlugin.FoodColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_provisions_food.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_provisions_food.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Injectors:
                        color = AmandsSensePlugin.InjectorsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_medical_injectors.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_medical_injectors.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.InjuryTreatment:
                        color = AmandsSensePlugin.InjuryTreatmentColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_medical_injury.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_medical_injury.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Medkits:
                        color = AmandsSensePlugin.MedkitsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_medical_medkits.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_medical_medkits.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Pills:
                        color = AmandsSensePlugin.PillsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_medical_pills.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_medical_pills.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.ElectronicKeys:
                        color = AmandsSensePlugin.ElectronicKeysColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_keys_electronic.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_keys_electronic.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.MechanicalKeys:
                        color = AmandsSensePlugin.MechanicalKeysColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_keys_mechanic.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_keys_mechanic.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.InfoItems:
                        if (observedLootItem.Item.QuestItem)
                            color = AmandsSensePlugin.QuestItemsColor.Value;
                        else
                            color = AmandsSensePlugin.InfoItemsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_info.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_info.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.SpecialEquipment:
                        color = AmandsSensePlugin.SpecialEquipmentColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_spec.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_spec.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Maps:
                        color = AmandsSensePlugin.MapsColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_maps.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_maps.png"];
                            useNewSize = true;
                        }

                        break;
                    case ESenseItemType.Money:
                        color = AmandsSensePlugin.MoneyColor.Value;
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_money.png"))
                        {
                            sprite = AmandsSenseClass.LoadedSprites["icon_money.png"];
                            useNewSize = true;
                        }

                        break;
                }

                if (AmandsSenseClass.itemsJsonClass != null)
                {
                    if (AmandsSenseClass.itemsJsonClass.KappaItems != null)
                        if (AmandsSenseClass.itemsJsonClass.KappaItems.Contains(observedLootItem.Item.TemplateId))
                            color = AmandsSensePlugin.KappaItemsColor.Value;
                    if (!observedLootItem.Item.CanSellOnRagfair &&
                        !AmandsSenseClass.itemsJsonClass.NonFleaExclude.Contains(observedLootItem.Item.TemplateId))
                        color = AmandsSensePlugin.NonFleaItemsColor.Value;
                    if (AmandsSenseClass.localPlayer != null && AmandsSenseClass.localPlayer.Profile != null &&
                        AmandsSenseClass.localPlayer.Profile.WishList != null &&
                        AmandsSenseClass.localPlayer.Profile.WishList.Contains(observedLootItem.Item.TemplateId))
                        color = AmandsSensePlugin.WishListItemsColor.Value;
                    if (AmandsSenseClass.itemsJsonClass.RareItems != null)
                        if (AmandsSenseClass.itemsJsonClass.RareItems.Contains(observedLootItem.Item.TemplateId))
                            color = AmandsSensePlugin.RareItemsColor.Value;
                }

                ItemSound = observedLootItem.Item.ItemSound;
                spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                if (spriteRenderer != null)
                {
                    light = gameObject.AddComponent<Light>();
                    if (light != null)
                    {
                        light.color = new Color(color.r, color.g, color.b, 1f);
                        light.shadows = LightShadows.None;
                        light.intensity = 0f;
                        light.range = AmandsSensePlugin.LightRange.Value;
                    }

                    spriteRenderer.sprite = sprite;
                    spriteRenderer.color = new Color(color.r, color.g, color.b, 0f);
                    transform.LookAt(Camera.main.transform.position, Vector3.up);
                    transform.localScale =
                        (useNewSize ? AmandsSensePlugin.NewSize.Value : AmandsSensePlugin.Size.Value) *
                        Mathf.Min(AmandsSensePlugin.SizeClamp.Value,
                            Vector3.Distance(Camera.main.transform.position, transform.position));
                    UpdateOpacity = true;
                    var itemClip = Singleton<GUISounds>.Instance.GetItemClip(ItemSound, EInventorySoundType.pickup);
                    if (itemClip != null)
                        Singleton<BetterAudio>.Instance.PlayAtPoint(transform.position, itemClip,
                            AmandsSensePlugin.AudioDistance.Value, BetterAudio.AudioSourceGroupType.Character,
                            AmandsSensePlugin.AudioRolloff.Value, AmandsSensePlugin.AudioVolume.Value,
                            EOcclusionTest.Regular);
                    await Task.Delay((int)(AmandsSensePlugin.Duration.Value * 1000));
                    UpdateOpacity = true;
                    StartOpacity = false;
                }
                else
                {
                    AmandsSenseClass.onItemsSensesRemove(Id);
                    AmandsSenseClass.ItemsSenses.Remove(Id);
                    Destroy(gameObject);
                }
            }
            else
            {
                AmandsSenseClass.onItemsSensesRemove(Id);
                AmandsSenseClass.ItemsSenses.Remove(Id);
                Destroy(gameObject);
            }
        }

        public void Update()
        {
            if (Camera.main != null)
            {
                transform.LookAt(Camera.main.transform.position, Vector3.up);
                transform.localScale = (useNewSize ? AmandsSensePlugin.NewSize.Value : AmandsSensePlugin.Size.Value) *
                                       Mathf.Min(AmandsSensePlugin.SizeClamp.Value,
                                           Vector3.Distance(Camera.main.transform.position, transform.position));
            }

            if (UpdateOpacity)
            {
                if (StartOpacity)
                {
                    Opacity += AmandsSensePlugin.OpacitySpeed.Value * Time.deltaTime;
                    if (Opacity >= 1f)
                    {
                        UpdateOpacity = false;
                        StartOpacity = false;
                    }
                }
                else
                {
                    Opacity -= AmandsSensePlugin.OpacitySpeed.Value * Time.deltaTime;
                    if (Opacity <= 0f)
                    {
                        UpdateOpacity = false;
                        AmandsSenseClass.ItemsSenses.Remove(Id);
                        Destroy(gameObject);
                    }
                }

                if (spriteRenderer != null && light != null)
                {
                    spriteRenderer.color = new Color(color.r, color.g, color.b, color.a * Opacity);
                    light.intensity = AmandsSensePlugin.LightIntensity.Value * Opacity;
                }
            }
        }
    }
}