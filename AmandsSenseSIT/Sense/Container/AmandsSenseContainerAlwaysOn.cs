﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using AmandSenseSIT.Types;
using EFT.Interactive;
using HarmonyLib;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AmandSenseSIT.Sense.Container
{
    public class AmandsSenseContainerAlwaysOn : MonoBehaviour
    {
        public Collider collider;
        public LootableContainer lootableContainer;
        public bool emptyLootableContainer = false;
        public int itemCount = 0;
        public string Id;
        public SpriteRenderer spriteRenderer;
        public Sprite sprite;
        public Light light;
        public Color color = AmandsSensePlugin.LootableContainerColor.Value;

        public float Delay = 0f;
        public bool UpdateOpacity = false;
        public bool StartOpacity = true;
        private float Opacity = 0f;

        public void Start()
        {
            if (AmandsSenseClass.ContainersAlwaysOn.Contains(this))
            {
                Destroy(gameObject);
            }
            else
            {
                AmandsSenseClass.ContainersAlwaysOn.Add(this);
                WaitAndStart();
            }
        }

        private async void WaitAndStart()
        {
            try
            {
                await Task.Delay((int)(Random.Range(0.0f, 1f) * 1000));
                if (gameObject != null && lootableContainer != null && lootableContainer.gameObject.activeSelf &&
                    AmandsSenseClass.localPlayer != null)
                {
                    var boxCollider = lootableContainer.gameObject.GetComponent<BoxCollider>();
                    if (boxCollider != null)
                    {
                        var position = boxCollider.transform.TransformPoint(boxCollider.center);
                        gameObject.transform.position = new Vector3(position.x,
                            boxCollider.ClosestPoint(position + Vector3.up * 100f).y +
                            AmandsSensePlugin.NormalSize.Value,
                            position.z);
                    }
                    else
                    {
                        gameObject.transform.position = lootableContainer.transform.position +
                                                        Vector3.up * AmandsSensePlugin.NormalSize.Value;
                    }

                    var eSenseItemColor = ESenseItemColor.Default;
                    if (lootableContainer.ItemOwner != null && AmandsSenseClass.itemsJsonClass != null &&
                        AmandsSenseClass.itemsJsonClass.RareItems != null &&
                        AmandsSenseClass.itemsJsonClass.KappaItems != null &&
                        AmandsSenseClass.itemsJsonClass.NonFleaExclude != null &&
                        AmandsSenseClass.localPlayer.Profile != null &&
                        AmandsSenseClass.localPlayer.Profile.WishList != null)
                    {
                        var lootItemClass = lootableContainer.ItemOwner.RootItem as CompoundItem;
                        if (lootItemClass != null)
                        {
                            var Grids = Traverse.Create(lootItemClass).Field("Grids").GetValue<object[]>();
                            if (Grids != null)
                                foreach (var grid in Grids)
                                {
                                    var Items = Traverse.Create(grid).Property("Items")
                                        .GetValue<IEnumerable<EFT.InventoryLogic.Item>>();
                                    if (Items != null)
                                        foreach (var item in Items)
                                        {
                                            itemCount += 1;
                                            if (AmandsSenseClass.itemsJsonClass.RareItems.Contains(item.TemplateId))
                                            {
                                                eSenseItemColor = ESenseItemColor.Rare;
                                            }
                                            else if (AmandsSenseClass.localPlayer.Profile.WishList
                                                         .Contains(item.TemplateId) &&
                                                     eSenseItemColor != ESenseItemColor.Rare)
                                            {
                                                eSenseItemColor = ESenseItemColor.WishList;
                                            }
                                            else if (item.Template != null && !item.Template.CanSellOnRagfair &&
                                                     !AmandsSenseClass.itemsJsonClass.NonFleaExclude
                                                         .Contains(item.TemplateId) &&
                                                     eSenseItemColor != ESenseItemColor.Rare &&
                                                     eSenseItemColor != ESenseItemColor.WishList)
                                            {
                                                if (!AmandsSensePlugin.NonFleaAmmo.Value && GClass2552
                                                        .TypeTable["5485a8684bdc2da71d8b4567"]
                                                        .IsAssignableFrom(item.GetType()))
                                                    continue;
                                                else
                                                    eSenseItemColor = ESenseItemColor.NonFlea;
                                            }
                                            else if (AmandsSenseClass.itemsJsonClass.KappaItems.Contains(
                                                         item.TemplateId) &&
                                                     eSenseItemColor == ESenseItemColor.Default)
                                            {
                                                eSenseItemColor = ESenseItemColor.Kappa;
                                            }
                                        }
                                }
                        }
                    }

                    if (itemCount != 0)
                    {
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("LootableContainer.png"))
                            sprite = AmandsSenseClass.LoadedSprites["LootableContainer.png"];
                        switch (eSenseItemColor)
                        {
                            case ESenseItemColor.Kappa:
                                color = AmandsSensePlugin.KappaItemsColor.Value;
                                break;
                            case ESenseItemColor.NonFlea:
                                color = AmandsSensePlugin.NonFleaItemsColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_barter.png"))
                                    sprite = AmandsSenseClass.LoadedSprites["icon_barter.png"];
                                break;
                            case ESenseItemColor.WishList:
                                color = AmandsSensePlugin.WishListItemsColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_fav_checked.png"))
                                    sprite = AmandsSenseClass.LoadedSprites["icon_fav_checked.png"];
                                break;
                            case ESenseItemColor.Rare:
                                color = AmandsSensePlugin.RareItemsColor.Value;
                                break;
                        }

                        spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                        if (spriteRenderer != null)
                        {
                            light = gameObject.AddComponent<Light>();
                            if (light != null)
                            {
                                light.color = new Color(color.r, color.g, color.b, 1f);
                                light.shadows = LightShadows.None;
                                light.intensity = 0.0f;
                                light.range = AmandsSensePlugin.LightRange.Value;
                            }

                            spriteRenderer.sprite = sprite;
                            spriteRenderer.color = new Color(color.r, color.g, color.b, 0f);
                            transform.LookAt(Camera.main.transform.position, Vector3.up);
                            transform.localScale = AmandsSensePlugin.AlwaysOnSize.Value;
                            UpdateOpacity = true;
                        }
                        else
                        {
                            AmandsSenseClass.ContainersAlwaysOn.Remove(this);
                            Destroy(gameObject);
                        }
                    }
                    else
                    {
                        AmandsSenseClass.ContainersAlwaysOn.Remove(this);
                        Destroy(gameObject);
                    }
                }
                else
                {
                    AmandsSenseClass.ContainersAlwaysOn.Remove(this);
                    Destroy(gameObject);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }

        public void Update()
        {
            try
            {
                if (Camera.main != null) transform.LookAt(Camera.main.transform.position, Vector3.up);
                if (UpdateOpacity)
                {
                    if (StartOpacity)
                    {
                        Opacity += AmandsSensePlugin.OpacitySpeed.Value * Time.deltaTime;
                        if (Opacity >= 1f)
                        {
                            UpdateOpacity = false;
                            StartOpacity = false;
                        }
                    }
                    else
                    {
                        Opacity -= AmandsSensePlugin.OpacitySpeed.Value * Time.deltaTime;
                        if (Opacity <= 0f)
                        {
                            UpdateOpacity = false;
                            AmandsSenseClass.ContainersAlwaysOn.Remove(this);
                            Destroy(gameObject);
                        }
                    }

                    if (spriteRenderer != null && light != null)
                    {
                        spriteRenderer.color = new Color(color.r, color.g, color.b, color.a * Opacity * 0.5f);
                        light.intensity = AmandsSensePlugin.LightIntensity.Value * Opacity * 0.5f;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }
    }
}