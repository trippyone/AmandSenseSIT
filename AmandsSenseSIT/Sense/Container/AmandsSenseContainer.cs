﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using AmandSenseSIT.Types;
using Comfort.Common;
using EFT.Interactive;
using HarmonyLib;
using UnityEngine;

namespace AmandSenseSIT.Sense.Container
{
    public class AmandsSenseContainer : MonoBehaviour
    {
        public LootableContainer lootableContainer;
        public bool emptyLootableContainer = false;
        public int itemCount = 0;
        public string Id;
        public SpriteRenderer spriteRenderer;
        public Sprite sprite;
        public Light light;
        public Color color = AmandsSensePlugin.LootableContainerColor.Value;
        public bool useNewSize = false;

        public float Delay = 0f;
        private bool UpdateOpacity = false;
        private bool StartOpacity = true;
        private float Opacity = 0f;

        public void Start()
        {
            try
            {
                if (AmandsSenseClass.ItemsSenses.Contains(Id))
                {
                    Destroy(gameObject);
                }
                else
                {
                    AmandsSenseClass.ItemsSenses.Add(Id);
                    WaitAndStart();
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }

        private async void WaitAndStart()
        {
            try
            {
                await Task.Delay((int)(Delay * 1000));
                if (lootableContainer != null && lootableContainer.gameObject.activeSelf &&
                    AmandsSenseClass.localPlayer != null &&
                    lootableContainer.transform.position.y >
                    AmandsSenseClass.localPlayer.Position.y + AmandsSensePlugin.MinHeight.Value &&
                    lootableContainer.transform.position.y <
                    AmandsSenseClass.localPlayer.Position.y + AmandsSensePlugin.MaxHeight.Value)
                {
                    var boxCollider = lootableContainer.gameObject.GetComponent<BoxCollider>();
                    if (boxCollider != null)
                    {
                        var position = boxCollider.transform.TransformPoint(boxCollider.center);
                        gameObject.transform.position = new Vector3(position.x,
                            boxCollider.ClosestPoint(position + Vector3.up * 100f).y +
                            AmandsSensePlugin.NormalSize.Value,
                            position.z);
                    }
                    else
                    {
                        gameObject.transform.position = lootableContainer.transform.position +
                                                        Vector3.up * AmandsSensePlugin.NormalSize.Value;
                    }

                    var eSenseItemColor = ESenseItemColor.Default;
                    if (lootableContainer.ItemOwner != null && AmandsSenseClass.itemsJsonClass != null &&
                        AmandsSenseClass.itemsJsonClass.RareItems != null &&
                        AmandsSenseClass.itemsJsonClass.KappaItems != null &&
                        AmandsSenseClass.itemsJsonClass.NonFleaExclude != null &&
                        AmandsSenseClass.localPlayer.Profile != null &&
                        AmandsSenseClass.localPlayer.Profile.WishList != null)
                    {
                        var lootItemClass = lootableContainer.ItemOwner.RootItem as CompoundItem;
                        if (lootItemClass != null)
                        {
                            var Grids = Traverse.Create(lootItemClass).Field("Grids").GetValue<object[]>();
                            if (Grids != null)
                                foreach (var grid in Grids)
                                {
                                    var Items = Traverse.Create(grid).Property("Items")
                                        .GetValue<IEnumerable<EFT.InventoryLogic.Item>>();
                                    if (Items != null)
                                        foreach (var item in Items)
                                        {
                                            itemCount += 1;
                                            if (AmandsSenseClass.itemsJsonClass.RareItems.Contains(item.TemplateId))
                                            {
                                                eSenseItemColor = ESenseItemColor.Rare;
                                            }
                                            else if (AmandsSenseClass.localPlayer.Profile.WishList
                                                         .Contains(item.TemplateId) &&
                                                     eSenseItemColor != ESenseItemColor.Rare)
                                            {
                                                eSenseItemColor = ESenseItemColor.WishList;
                                            }
                                            else if (item.Template != null && !item.Template.CanSellOnRagfair &&
                                                     !AmandsSenseClass.itemsJsonClass.NonFleaExclude
                                                         .Contains(item.TemplateId) &&
                                                     eSenseItemColor != ESenseItemColor.Rare &&
                                                     eSenseItemColor != ESenseItemColor.WishList)
                                            {
                                                if (!AmandsSensePlugin.NonFleaAmmo.Value && GClass2552
                                                        .TypeTable["5485a8684bdc2da71d8b4567"]
                                                        .IsAssignableFrom(item.GetType()))
                                                    continue;
                                                else
                                                    eSenseItemColor = ESenseItemColor.NonFlea;
                                            }
                                            else if (AmandsSenseClass.itemsJsonClass.KappaItems.Contains(
                                                         item.TemplateId) &&
                                                     eSenseItemColor == ESenseItemColor.Default)
                                            {
                                                eSenseItemColor = ESenseItemColor.Kappa;
                                            }
                                        }
                                }
                        }
                    }

                    if (itemCount != 0)
                    {
                        AmandsSenseClass.onContainerSensesAdded(Id, gameObject.transform.position);
                        if (AmandsSenseClass.LoadedSprites.ContainsKey("LootableContainer.png"))
                            sprite = AmandsSenseClass.LoadedSprites["LootableContainer.png"];
                        switch (eSenseItemColor)
                        {
                            case ESenseItemColor.Kappa:
                                color = AmandsSensePlugin.KappaItemsColor.Value;
                                break;
                            case ESenseItemColor.NonFlea:
                                color = AmandsSensePlugin.NonFleaItemsColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_barter.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_barter.png"];
                                    useNewSize = true;
                                }

                                break;
                            case ESenseItemColor.WishList:
                                color = AmandsSensePlugin.WishListItemsColor.Value;
                                if (AmandsSenseClass.LoadedSprites.ContainsKey("icon_fav_checked.png"))
                                {
                                    sprite = AmandsSenseClass.LoadedSprites["icon_fav_checked.png"];
                                    useNewSize = true;
                                }

                                break;
                            case ESenseItemColor.Rare:
                                color = AmandsSensePlugin.RareItemsColor.Value;
                                break;
                        }

                        spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                        if (spriteRenderer != null)
                        {
                            light = gameObject.AddComponent<Light>();
                            if (light != null)
                            {
                                light.color = new Color(color.r, color.g, color.b, 1f);
                                light.shadows = LightShadows.None;
                                light.intensity = 0f;
                                light.range = AmandsSensePlugin.LightRange.Value;
                            }

                            spriteRenderer.sprite = sprite;
                            spriteRenderer.color = new Color(color.r, color.g, color.b, 0f);
                            transform.LookAt(Camera.main.transform.position, Vector3.up);
                            transform.localScale =
                                (useNewSize ? AmandsSensePlugin.NewSize.Value : AmandsSensePlugin.Size.Value) *
                                Mathf.Min(AmandsSensePlugin.SizeClamp.Value,
                                    Vector3.Distance(Camera.main.transform.position, transform.position));
                            UpdateOpacity = true;
                            if (lootableContainer.OpenSound.Length > 0)
                            {
                                var OpenSound = lootableContainer.OpenSound[0];
                                if (OpenSound != null)
                                    Singleton<BetterAudio>.Instance.PlayAtPoint(transform.position, OpenSound,
                                        AmandsSensePlugin.AudioDistance.Value,
                                        BetterAudio.AudioSourceGroupType.Character,
                                        AmandsSensePlugin.AudioRolloff.Value, AmandsSensePlugin.AudioVolume.Value,
                                        EOcclusionTest.Regular);
                            }

                            await Task.Delay((int)(AmandsSensePlugin.Duration.Value * 1000));
                            UpdateOpacity = true;
                            StartOpacity = false;
                        }
                        else
                        {
                            AmandsSenseClass.ItemsSenses.Remove(Id);
                            Destroy(gameObject);
                        }
                    }
                    else
                    {
                        AmandsSenseClass.ItemsSenses.Remove(Id);
                        Destroy(gameObject);
                    }
                }
                else
                {
                    AmandsSenseClass.ItemsSenses.Remove(Id);
                    Destroy(gameObject);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }

        public void Update()
        {
            try
            {
                if (Camera.main != null)
                {
                    transform.LookAt(Camera.main.transform.position, Vector3.up);
                    transform.localScale = (useNewSize ? AmandsSensePlugin.NewSize.Value : AmandsSensePlugin.Size.Value) *
                                           Mathf.Min(AmandsSensePlugin.SizeClamp.Value,
                                               Vector3.Distance(Camera.main.transform.position, transform.position));
                }

                if (UpdateOpacity)
                {
                    if (StartOpacity)
                    {
                        Opacity += AmandsSensePlugin.OpacitySpeed.Value * Time.deltaTime;
                        if (Opacity >= 1f)
                        {
                            UpdateOpacity = false;
                            StartOpacity = false;
                        }
                    }
                    else
                    {
                        Opacity -= AmandsSensePlugin.OpacitySpeed.Value * Time.deltaTime;
                        if (Opacity <= 0f)
                        {
                            UpdateOpacity = false;
                            AmandsSenseClass.ItemsSenses.Remove(Id);
                            Destroy(gameObject);
                        }
                    }

                    if (spriteRenderer != null && light != null)
                    {
                        spriteRenderer.color = new Color(color.r, color.g, color.b, color.a * Opacity);
                        light.intensity = AmandsSensePlugin.LightIntensity.Value * Opacity;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }
    }
}