using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using AmandSenseSIT.Sense;
using AmandSenseSIT.Sense.Container;
using AmandSenseSIT.Sense.DeadBody;
using AmandSenseSIT.Sense.Item;
using AmandSenseSIT.Types;
using EFT;
using EFT.Interactive;
using HarmonyLib;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using TemplateIdToObjectMappingsClass = GClass2552;

namespace AmandSenseSIT
{
    public class AmandsSenseClass : MonoBehaviour
    {
        public static LocalPlayer localPlayer;

        public static LayerMask SphereInteractiveLayerMask = LayerMask.GetMask("Interactive");
        public static LayerMask SphereDeadbodyLayerMask = LayerMask.GetMask("Deadbody");

        public static Dictionary<string, Sprite> LoadedSprites = new Dictionary<string, Sprite>();
        public static Dictionary<string, AudioClip> LoadedAudioClips = new Dictionary<string, AudioClip>();

        public static List<string> ItemsSenses = new List<string>();
        public static List<AmandsSenseAlwaysOn> ItemsAlwaysOn = new List<AmandsSenseAlwaysOn>();
        public static List<AmandsSenseContainerAlwaysOn> ContainersAlwaysOn = new List<AmandsSenseContainerAlwaysOn>();
        public static List<AmandsSenseDeadbodyAlwaysOn> DeadbodyAlwaysOn = new List<AmandsSenseDeadbodyAlwaysOn>();

        public static float CooldownTime = 0f;

        public delegate void ItemsSensesAdded(string Id, string TemplateId, bool CanSellOnRagfair, Vector3 position,
            ESenseItemType SenseItemType);

        public static ItemsSensesAdded onItemsSensesAdded;

        public delegate void ItemsSensesRemove(string Id);

        public static ItemsSensesRemove onItemsSensesRemove;

        public delegate void ContainerSensesAdded(string Id, Vector3 position);

        public static ContainerSensesAdded onContainerSensesAdded;
        public static float Radius = 0f;

        public static PrismEffects prismEffects;

        public static ItemsJsonClass itemsJsonClass;

        public static float lastDoubleClickTime;

        public static bool ValidSenseAlwaysOn = false;

        public void Start()
        {
            try
            {
                itemsJsonClass =
                    ReadFromJsonFile<ItemsJsonClass>(AppDomain.CurrentDomain.BaseDirectory +
                                                     "/BepInEx/plugins/Sense/Items.json");
                ReloadFiles();
                onItemsSensesAdded += ItemsSensesAddedMethod;
                onItemsSensesRemove += ItemsSensesRemoveMethod;
                onContainerSensesAdded += ContainerSensesAddedMethod;
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }

        public void ItemsSensesAddedMethod(string Id, string TemplateId, bool CanSellOnRagfair, Vector3 position,
            ESenseItemType SenseItemType)
        {
        }

        public void ItemsSensesRemoveMethod(string Id)
        {
        }

        public void ContainerSensesAddedMethod(string Id, Vector3 position)
        {
        }

        public void Update()
        {
            try
            {
                if (localPlayer != null && AmandsSensePlugin.EnableSense.Value)
                {
                    if (CooldownTime < AmandsSensePlugin.Cooldown.Value) CooldownTime += Time.deltaTime;
                    if (Input.GetKeyDown(AmandsSensePlugin.SenseKey.Value.MainKey))
                    {
                        if (AmandsSensePlugin.DoubleClick.Value)
                        {
                            var timeSinceLastClick = Time.time - lastDoubleClickTime;
                            lastDoubleClickTime = Time.time;
                            if (timeSinceLastClick <= AmandsSensePlugin.DoubleClickDelay.Value &&
                                CooldownTime >= AmandsSensePlugin.Cooldown.Value) Sense();
                        }
                        else
                        {
                            if (CooldownTime >= AmandsSensePlugin.Cooldown.Value) Sense();
                        }
                    }

                    if (Radius < Mathf.Max(AmandsSensePlugin.Radius.Value, AmandsSensePlugin.DeadbodyRadius.Value))
                    {
                        Radius += AmandsSensePlugin.Speed.Value * Time.deltaTime;
                        if (prismEffects != null)
                        {
                            prismEffects.dofFocusPoint = Radius - prismEffects.dofFocusDistance;
                            if (prismEffects.dofRadius < AmandsSensePlugin.dofRadius.Value)
                                prismEffects.dofRadius += AmandsSensePlugin.dofRadiusStartSpeed.Value * Time.deltaTime;
                        }
                    }
                    else if (prismEffects != null && prismEffects.dofRadius > 0.001f)
                    {
                        prismEffects.dofRadius -= AmandsSensePlugin.dofRadiusEndSpeed.Value * Time.deltaTime;
                        if (prismEffects.dofRadius < 0.001f) prismEffects.useDof = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }

        public void Sense()
        {
            if (gameObject != null)
            {
                if (localPlayer != null)
                {
                    CooldownTime = 0;
                    var colliders = new Collider[AmandsSensePlugin.Limit.Value];
                    var colliderCount = Physics.OverlapSphereNonAlloc(localPlayer.Position,
                        AmandsSensePlugin.Radius.Value, colliders, SphereInteractiveLayerMask,
                        QueryTriggerInteraction.Collide);
                    for (var i = 0; i < colliderCount; i++)
                    {
                        Component component = colliders[i].transform.gameObject.GetComponent<ObservedLootItem>();
                        if (component != null)
                        {
                            var observedLootItem = component as ObservedLootItem;
                            if (observedLootItem != null)
                            {
                                var SenseItemGameObject = new GameObject("SenseItem");
                                var amandsSenseItem = SenseItemGameObject.AddComponent<AmandsSenseItem>();
                                amandsSenseItem.observedLootItem = observedLootItem;
                                amandsSenseItem.Id = observedLootItem.ItemId;
                                amandsSenseItem.Delay =
                                    Vector3.Distance(localPlayer.Position, observedLootItem.transform.position) /
                                    AmandsSensePlugin.Speed.Value;
                            }
                        }
                        else
                        {
                            component = colliders[i].transform.gameObject.GetComponent<LootableContainer>();
                            if (component != null)
                            {
                                var lootableContainer = component as LootableContainer;
                                if (lootableContainer != null)
                                {
                                    var SenseContainerGameObject = new GameObject("SenseContainer");
                                    var amandsSenseContainer =
                                        SenseContainerGameObject.AddComponent<AmandsSenseContainer>();
                                    amandsSenseContainer.lootableContainer = lootableContainer;
                                    amandsSenseContainer.Id = lootableContainer.Id;
                                    amandsSenseContainer.Delay =
                                        Vector3.Distance(localPlayer.Position, lootableContainer.transform.position) /
                                        AmandsSensePlugin.Speed.Value;
                                }
                            }
                        }
                    }

                    var players = new List<string>();
                    var colliders2 = new Collider[AmandsSensePlugin.Limit.Value];
                    var colliderCount2 = Physics.OverlapSphereNonAlloc(localPlayer.Position,
                        AmandsSensePlugin.DeadbodyRadius.Value, colliders2, SphereDeadbodyLayerMask,
                        QueryTriggerInteraction.Collide);
                    for (var i = 0; i < colliderCount2; i++)
                    {
                        var bodyPartCollider = colliders2[i].transform.gameObject.GetComponent<BodyPartCollider>();
                        if (bodyPartCollider != null && bodyPartCollider.BodyPartType == EBodyPart.Chest)
                        {
                            var player = Traverse.Create(bodyPartCollider).Field("Player").GetValue() as Player;
                            if (player == null)
                                player = Traverse.Create(bodyPartCollider).Property("Player").GetValue() as Player;
                            if (player != null && !players.Contains(player.ProfileId))
                            {
                                players.Add(player.ProfileId);
                                var corpse = player.GetComponent<Corpse>();
                                if (corpse != null)
                                {
                                    var SenseDeadbodyGameObject = new GameObject("SenseDeadbody");
                                    var amandsSenseDeadbody =
                                        SenseDeadbodyGameObject.AddComponent<AmandsSenseDeadbody>();
                                    amandsSenseDeadbody.corpse = corpse;
                                    amandsSenseDeadbody.bodyPartCollider = bodyPartCollider;
                                    amandsSenseDeadbody.Id = corpse.ItemId;
                                    if (bodyPartCollider.Collider != null)
                                        amandsSenseDeadbody.Delay = Vector3.Distance(localPlayer.Position,
                                                                        bodyPartCollider.Collider.transform.position) /
                                                                    AmandsSensePlugin.Speed.Value;
                                    else
                                        amandsSenseDeadbody.Delay =
                                            Vector3.Distance(localPlayer.Position, corpse.transform.position) /
                                            AmandsSensePlugin.Speed.Value;
                                }
                            }
                        }
                    }
                }

                if (prismEffects != null)
                {
                    Radius = 0;
                    prismEffects.useDof = AmandsSensePlugin.useDof.Value;
                    prismEffects.debugDofPass = false;
                    prismEffects.dofForceEnableMedian = AmandsSensePlugin.dofForceEnableMedian.Value;
                    prismEffects.dofBokehFactor = AmandsSensePlugin.dofBokehFactor.Value;
                    prismEffects.dofFocusDistance = AmandsSensePlugin.dofFocusDistance.Value;
                    prismEffects.dofNearFocusDistance = 100f;
                    prismEffects.dofRadius = 0f;
                }
            }
        }

        public async void DynamicAlwaysOnSense()
        {
            try
            {
                await Task.Delay((int)(AmandsSensePlugin.AlwaysOnFrequency.Value * 1000));
                if (!AmandsSensePlugin.SenseAlwaysOn.Value)
                {
                    if (ValidSenseAlwaysOn)
                    {
                        ValidSenseAlwaysOn = false;
                        ClearAlwaysOn();
                    }

                    DynamicAlwaysOnSense();
                    return;
                }

                if (localPlayer == null) return;
                var colliders = new Collider[AmandsSensePlugin.Limit.Value];
                var colliderCount = Physics.OverlapSphereNonAlloc((Vector3)localPlayer.Position,
                    AmandsSensePlugin.AlwaysOnRadius.Value, colliders, SphereInteractiveLayerMask,
                    QueryTriggerInteraction.Collide);

                await Task.Delay((int)100);
                var collidersList = new List<Collider>(colliders);
                foreach (var amandsSenseAlwaysOn in ItemsAlwaysOn)
                {
                    if (amandsSenseAlwaysOn == null) continue;
                    if (amandsSenseAlwaysOn.collider != null)
                    {
                        var index = collidersList.IndexOf(amandsSenseAlwaysOn.collider);
                        if (index != -1)
                        {
                            collidersList[index] = null;
                        }
                        else
                        {
                            amandsSenseAlwaysOn.StartOpacity = false;
                            amandsSenseAlwaysOn.UpdateOpacity = true;
                        }
                    }
                    else
                    {
                        amandsSenseAlwaysOn.StartOpacity = false;
                        amandsSenseAlwaysOn.UpdateOpacity = true;
                    }
                }

                await Task.Delay((int)100);
                foreach (var amandsSenseContainerAlwaysOn in ContainersAlwaysOn)
                {
                    if (amandsSenseContainerAlwaysOn == null) continue;
                    if (amandsSenseContainerAlwaysOn.collider != null)
                    {
                        var index = collidersList.IndexOf(amandsSenseContainerAlwaysOn.collider);
                        if (index != -1)
                        {
                            collidersList[index] = null;
                        }
                        else
                        {
                            amandsSenseContainerAlwaysOn.StartOpacity = false;
                            amandsSenseContainerAlwaysOn.UpdateOpacity = true;
                        }
                    }
                    else
                    {
                        amandsSenseContainerAlwaysOn.StartOpacity = false;
                        amandsSenseContainerAlwaysOn.UpdateOpacity = true;
                    }
                }

                await Task.Delay(100);
                for (var i = 0; i < colliderCount; i++)
                {
                    if (collidersList[i] == null) continue;
                    Component component = collidersList[i].transform.gameObject.GetComponent<ObservedLootItem>();
                    if (component != null)
                    {
                        var observedLootItem = component as ObservedLootItem;
                        if (observedLootItem != null)
                        {
                            var SenseAlwaysOnGameObject = new GameObject("SenseAlwaysOn");
                            var amandsSenseAlwaysOn = SenseAlwaysOnGameObject.AddComponent<AmandsSenseAlwaysOn>();
                            amandsSenseAlwaysOn.collider = collidersList[i];
                            amandsSenseAlwaysOn.observedLootItem = observedLootItem;
                            amandsSenseAlwaysOn.Id = observedLootItem.ItemId;
                            //amandsSenseAlwaysOn.Delay = UnityEngine.Random.Range(0.0f, 1f);
                        }
                    }
                    else
                    {
                        component = collidersList[i].transform.gameObject.GetComponent<LootableContainer>();
                        if (component != null)
                        {
                            var lootableContainer = component as LootableContainer;
                            if (lootableContainer != null)
                            {
                                var SenseContainerAlwaysOnGameObject = new GameObject("SenseContainerAlwaysOn");
                                var amandsSenseContainerAlwaysOn = SenseContainerAlwaysOnGameObject
                                    .AddComponent<AmandsSenseContainerAlwaysOn>();
                                amandsSenseContainerAlwaysOn.collider = collidersList[i];
                                amandsSenseContainerAlwaysOn.lootableContainer = lootableContainer;
                                amandsSenseContainerAlwaysOn.Id = lootableContainer.Id;
                                //amandsSenseContainerAlwaysOn.Delay = UnityEngine.Random.Range(0.0f, 1f);
                            }
                        }
                    }
                }

                await Task.Delay((int)100);
                var players = new List<string>();
                var colliders2 = new Collider[AmandsSensePlugin.Limit.Value];
                var colliderCount2 = Physics.OverlapSphereNonAlloc(localPlayer.Position,
                    AmandsSensePlugin.AlwaysOnDeadbodyRadius.Value, colliders2, SphereDeadbodyLayerMask,
                    QueryTriggerInteraction.Collide);

                await Task.Delay((int)100);
                var colliders2List = new List<Collider>(colliders2);
                foreach (var amandsSenseDeadbodyAlwaysOn in DeadbodyAlwaysOn)
                {
                    if (amandsSenseDeadbodyAlwaysOn == null) continue;
                    if (amandsSenseDeadbodyAlwaysOn.collider != null)
                    {
                        var index = colliders2List.IndexOf(amandsSenseDeadbodyAlwaysOn.collider);
                        if (index != -1)
                        {
                            colliders2List[index] = null;
                        }
                        else
                        {
                            amandsSenseDeadbodyAlwaysOn.StartOpacity = false;
                            amandsSenseDeadbodyAlwaysOn.UpdateOpacity = true;
                        }
                    }
                    else
                    {
                        amandsSenseDeadbodyAlwaysOn.StartOpacity = false;
                        amandsSenseDeadbodyAlwaysOn.UpdateOpacity = true;
                    }
                }

                await Task.Delay((int)100);
                for (var i = 0; i < colliderCount2; i++)
                {
                    if (colliders2List[i] == null) continue;
                    var bodyPartCollider = colliders2List[i].transform.gameObject.GetComponent<BodyPartCollider>();
                    if (bodyPartCollider != null && bodyPartCollider.BodyPartType == EBodyPart.Stomach)
                    {
                        var player = Traverse.Create(bodyPartCollider).Field("Player").GetValue() as Player;
                        if (player == null)
                            player = Traverse.Create(bodyPartCollider).Property("Player").GetValue() as Player;
                        if (player != null && !players.Contains(player.ProfileId))
                        {
                            players.Add(player.ProfileId);
                            var corpse = player.GetComponent<Corpse>();
                            if (corpse != null)
                            {
                                var SenseDeadbodyAlwaysOnGameObject = new GameObject("SenseDeadbodyAlwaysOn");
                                var amandsSenseDeadbodyAlwaysOn =
                                    SenseDeadbodyAlwaysOnGameObject.AddComponent<AmandsSenseDeadbodyAlwaysOn>();
                                amandsSenseDeadbodyAlwaysOn.collider = colliders2List[i];
                                amandsSenseDeadbodyAlwaysOn.corpse = corpse;
                                amandsSenseDeadbodyAlwaysOn.bodyPartCollider = bodyPartCollider;
                                amandsSenseDeadbodyAlwaysOn.Id = corpse.ItemId;
                                //amandsSenseDeadbodyAlwaysOn.Delay = UnityEngine.Random.Range(0.0f, 1f);
                            }
                        }
                    }
                }

                ValidSenseAlwaysOn = true;
                DynamicAlwaysOnSense();
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }

        public void ClearAlwaysOn()
        {
            try
            {
                foreach (var amandsSenseAlwaysOn in ItemsAlwaysOn)
                {
                    if (amandsSenseAlwaysOn == null) continue;
                    amandsSenseAlwaysOn.StartOpacity = false;
                    amandsSenseAlwaysOn.UpdateOpacity = true;
                }

                foreach (var amandsSenseContainerAlwaysOn in ContainersAlwaysOn)
                {
                    if (amandsSenseContainerAlwaysOn == null) continue;
                    amandsSenseContainerAlwaysOn.StartOpacity = false;
                    amandsSenseContainerAlwaysOn.UpdateOpacity = true;
                }

                foreach (var amandsSenseDeadbodyAlwaysOn in DeadbodyAlwaysOn)
                {
                    if (amandsSenseDeadbodyAlwaysOn == null) continue;
                    amandsSenseDeadbodyAlwaysOn.StartOpacity = false;
                    amandsSenseDeadbodyAlwaysOn.UpdateOpacity = true;
                }

                ItemsAlwaysOn.Clear();
                ContainersAlwaysOn.Clear();
                DeadbodyAlwaysOn.Clear();
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }

        public static ESenseItemType SenseItemType(Type itemType)
        {
            if (TemplateIdToObjectMappingsClass.TypeTable["57864ada245977548638de91"].IsAssignableFrom(itemType))
                return ESenseItemType.BuildingMaterials;
            if (TemplateIdToObjectMappingsClass.TypeTable["57864a66245977548f04a81f"].IsAssignableFrom(itemType))
                return ESenseItemType.Electronics;
            if (TemplateIdToObjectMappingsClass.TypeTable["57864ee62459775490116fc1"].IsAssignableFrom(itemType))
                return ESenseItemType.EnergyElements;
            if (TemplateIdToObjectMappingsClass.TypeTable["57864e4c24597754843f8723"].IsAssignableFrom(itemType))
                return ESenseItemType.FlammableMaterials;
            if (TemplateIdToObjectMappingsClass.TypeTable["57864c322459775490116fbf"].IsAssignableFrom(itemType))
                return ESenseItemType.HouseholdMaterials;
            if (TemplateIdToObjectMappingsClass.TypeTable["57864c8c245977548867e7f1"].IsAssignableFrom(itemType))
                return ESenseItemType.MedicalSupplies;
            if (TemplateIdToObjectMappingsClass.TypeTable["57864bb7245977548b3b66c2"].IsAssignableFrom(itemType))
                return ESenseItemType.Tools;
            if (TemplateIdToObjectMappingsClass.TypeTable["57864a3d24597754843f8721"].IsAssignableFrom(itemType))
                return ESenseItemType.Valuables;
            if (TemplateIdToObjectMappingsClass.TypeTable["590c745b86f7743cc433c5f2"].IsAssignableFrom(itemType))
                return ESenseItemType.Others;
            if (TemplateIdToObjectMappingsClass.TypeTable["5448e53e4bdc2d60728b4567"].IsAssignableFrom(itemType))
                return ESenseItemType.Backpacks;
            if (TemplateIdToObjectMappingsClass.TypeTable["5448e54d4bdc2dcc718b4568"].IsAssignableFrom(itemType))
                return ESenseItemType.BodyArmor;
            if (TemplateIdToObjectMappingsClass.TypeTable["5448e5724bdc2ddf718b4568"].IsAssignableFrom(itemType))
                return ESenseItemType.Eyewear;
            if (TemplateIdToObjectMappingsClass.TypeTable["5a341c4686f77469e155819e"].IsAssignableFrom(itemType))
                return ESenseItemType.Facecovers;
            if (TemplateIdToObjectMappingsClass.TypeTable["5a341c4086f77401f2541505"].IsAssignableFrom(itemType))
                return ESenseItemType.Headgear;
            if (TemplateIdToObjectMappingsClass.TypeTable["57bef4c42459772e8d35a53b"].IsAssignableFrom(itemType))
                return ESenseItemType.GearComponents;
            if (TemplateIdToObjectMappingsClass.TypeTable["5b3f15d486f77432d0509248"].IsAssignableFrom(itemType))
                return ESenseItemType.GearComponents;
            if (TemplateIdToObjectMappingsClass.TypeTable["5645bcb74bdc2ded0b8b4578"].IsAssignableFrom(itemType))
                return ESenseItemType.Headsets;
            if (TemplateIdToObjectMappingsClass.TypeTable["5448bf274bdc2dfc2f8b456a"].IsAssignableFrom(itemType))
                return ESenseItemType.SecureContainers;
            if (TemplateIdToObjectMappingsClass.TypeTable["5795f317245977243854e041"].IsAssignableFrom(itemType))
                return ESenseItemType.StorageContainers;
            if (TemplateIdToObjectMappingsClass.TypeTable["5448e5284bdc2dcb718b4567"].IsAssignableFrom(itemType))
                return ESenseItemType.TacticalRigs;
            if (TemplateIdToObjectMappingsClass.TypeTable["550aa4154bdc2dd8348b456b"].IsAssignableFrom(itemType))
                return ESenseItemType.FunctionalMods;
            if (TemplateIdToObjectMappingsClass.TypeTable["55802f3e4bdc2de7118b4584"].IsAssignableFrom(itemType))
                return ESenseItemType.GearMods;
            if (TemplateIdToObjectMappingsClass.TypeTable["5a74651486f7744e73386dd1"].IsAssignableFrom(itemType))
                return ESenseItemType.GearMods;
            if (TemplateIdToObjectMappingsClass.TypeTable["55802f4a4bdc2ddb688b4569"].IsAssignableFrom(itemType))
                return ESenseItemType.VitalParts;
            if (TemplateIdToObjectMappingsClass.TypeTable["5447e1d04bdc2dff2f8b4567"].IsAssignableFrom(itemType))
                return ESenseItemType.MeleeWeapons;
            if (TemplateIdToObjectMappingsClass.TypeTable["543be6564bdc2df4348b4568"].IsAssignableFrom(itemType))
                return ESenseItemType.Throwables;
            if (TemplateIdToObjectMappingsClass.TypeTable["543be5cb4bdc2deb348b4568"].IsAssignableFrom(itemType))
                return ESenseItemType.AmmoPacks;
            if (TemplateIdToObjectMappingsClass.TypeTable["5485a8684bdc2da71d8b4567"].IsAssignableFrom(itemType))
                return ESenseItemType.Rounds;
            if (TemplateIdToObjectMappingsClass.TypeTable["5448e8d64bdc2dce718b4568"].IsAssignableFrom(itemType))
                return ESenseItemType.Drinks;
            if (TemplateIdToObjectMappingsClass.TypeTable["5448e8d04bdc2ddf718b4569"].IsAssignableFrom(itemType))
                return ESenseItemType.Food;
            if (TemplateIdToObjectMappingsClass.TypeTable["5448f3a64bdc2d60728b456a"].IsAssignableFrom(itemType))
                return ESenseItemType.Injectors;
            if (TemplateIdToObjectMappingsClass.TypeTable["5448f3ac4bdc2dce718b4569"].IsAssignableFrom(itemType))
                return ESenseItemType.InjuryTreatment;
            if (TemplateIdToObjectMappingsClass.TypeTable["5448f39d4bdc2d0a728b4568"].IsAssignableFrom(itemType))
                return ESenseItemType.Medkits;
            if (TemplateIdToObjectMappingsClass.TypeTable["5448f3a14bdc2d27728b4569"].IsAssignableFrom(itemType))
                return ESenseItemType.Pills;
            if (TemplateIdToObjectMappingsClass.TypeTable["5c164d2286f774194c5e69fa"].IsAssignableFrom(itemType))
                return ESenseItemType.ElectronicKeys;
            if (TemplateIdToObjectMappingsClass.TypeTable["5c99f98d86f7745c314214b3"].IsAssignableFrom(itemType))
                return ESenseItemType.MechanicalKeys;
            if (TemplateIdToObjectMappingsClass.TypeTable["5448ecbe4bdc2d60728b4568"].IsAssignableFrom(itemType))
                return ESenseItemType.InfoItems;
            if (TemplateIdToObjectMappingsClass.TypeTable["5447e0e74bdc2d3c308b4567"].IsAssignableFrom(itemType))
                return ESenseItemType.SpecialEquipment;
            if (TemplateIdToObjectMappingsClass.TypeTable["616eb7aea207f41933308f46"].IsAssignableFrom(itemType))
                return ESenseItemType.SpecialEquipment;
            if (TemplateIdToObjectMappingsClass.TypeTable["61605ddea09d851a0a0c1bbc"].IsAssignableFrom(itemType))
                return ESenseItemType.SpecialEquipment;
            if (TemplateIdToObjectMappingsClass.TypeTable["5f4fbaaca5573a5ac31db429"].IsAssignableFrom(itemType))
                return ESenseItemType.SpecialEquipment;
            if (TemplateIdToObjectMappingsClass.TypeTable["567849dd4bdc2d150f8b456e"].IsAssignableFrom(itemType))
                return ESenseItemType.Maps;
            if (TemplateIdToObjectMappingsClass.TypeTable["543be5dd4bdc2deb348b4569"].IsAssignableFrom(itemType))
                return ESenseItemType.Money;
            return ESenseItemType.All;
        }

        public static void WriteToJsonFile<T>(string filePath, T objectToWrite, bool append = false) where T : new()
        {
            TextWriter writer = null;
            try
            {
                var contentsToWriteToFile = JsonConvert.SerializeObject(objectToWrite);
                writer = new StreamWriter(filePath, append);
                writer.Write(contentsToWriteToFile);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        public static T ReadFromJsonFile<T>(string filePath) where T : new()
        {
            TextReader reader = null;
            try
            {
                reader = new StreamReader(filePath);
                var fileContents = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(fileContents);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        public static void ReloadFiles()
        {
            var Files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + "/BepInEx/plugins/Sense/images/",
                "*.png");
            foreach (var File in Files) LoadSprite(File);
            var AudioFiles =
                Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + "/BepInEx/plugins/Sense/sounds/");
            foreach (var File in AudioFiles) LoadAudioClip(File);
        }

        private static async void LoadSprite(string path)
        {
            LoadedSprites[Path.GetFileName(path)] = await RequestSprite(path);
        }

        private static async Task<Sprite> RequestSprite(string path)
        {
            var www = UnityWebRequestTexture.GetTexture(path);
            var SendWeb = www.SendWebRequest();

            while (!SendWeb.isDone)
                await Task.Yield();

            if (www.isNetworkError || www.isHttpError)
            {
                return null;
            }
            else
            {
                var texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height),
                    new Vector2(0.5f, 0.5f));

                return sprite;
            }
        }

        private static async void LoadAudioClip(string path)
        {
            LoadedAudioClips[Path.GetFileName(path)] = await RequestAudioClip(path);
        }

        private static async Task<AudioClip> RequestAudioClip(string path)
        {
            var extension = Path.GetExtension(path);
            var audioType = AudioType.WAV;
            switch (extension)
            {
                case ".wav":
                    audioType = AudioType.WAV;
                    break;
                case ".ogg":
                    audioType = AudioType.OGGVORBIS;
                    break;
            }

            var www = UnityWebRequestMultimedia.GetAudioClip(path, audioType);
            var SendWeb = www.SendWebRequest();

            while (!SendWeb.isDone)
                await Task.Yield();

            if (www.isNetworkError || www.isHttpError)
            {
                return null;
            }
            else
            {
                var audioclip = DownloadHandlerAudioClip.GetContent(www);
                return audioclip;
            }
        }
    }
}