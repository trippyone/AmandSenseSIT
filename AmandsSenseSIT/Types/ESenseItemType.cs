﻿namespace AmandSenseSIT.Types
{
    public enum ESenseItemType
    {
        All,
        ObservedLootItem,
        Others,
        BuildingMaterials,
        Electronics,
        EnergyElements,
        FlammableMaterials,
        HouseholdMaterials,
        MedicalSupplies,
        Tools,
        Valuables,
        Backpacks,
        BodyArmor,
        Eyewear,
        Facecovers,
        GearComponents,
        Headgear,
        Headsets,
        SecureContainers,
        StorageContainers,
        TacticalRigs,
        FunctionalMods,
        GearMods,
        VitalParts,
        AssaultCarbines,
        AssaultRifles,
        BoltActionRifles,
        GrenadeLaunchers,
        MachineGuns,
        MarksmanRifles,
        MeleeWeapons,
        Pistols,
        SMGs,
        Shotguns,
        SpecialWeapons,
        Throwables,
        AmmoPacks,
        Rounds,
        Drinks,
        Food,
        Injectors,
        InjuryTreatment,
        Medkits,
        Pills,
        ElectronicKeys,
        MechanicalKeys,
        InfoItems,
        QuestItems,
        SpecialEquipment,
        Maps,
        Money
    }
}