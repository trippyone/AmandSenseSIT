﻿namespace AmandSenseSIT.Types
{
    public enum ESenseItemColor
    {
        Default,
        Kappa,
        NonFlea,
        WishList,
        Rare
    }
}