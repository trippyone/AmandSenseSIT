﻿using System.Collections.Generic;

namespace AmandSenseSIT.Types
{
    public class ItemsJsonClass
    {
        public List<string> RareItems { get; set; }
        public List<string> KappaItems { get; set; }
        public List<string> NonFleaExclude { get; set; }
    }
}